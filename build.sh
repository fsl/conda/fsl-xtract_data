#!/usr/bin/env bash

set -e

mkdir -p      $PREFIX/data/xtract_data
cp -r *       $PREFIX/data/xtract_data/
chmod -R 0755 $PREFIX/data/xtract_data/*
